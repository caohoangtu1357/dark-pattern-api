from django.shortcuts import render
from django.http import HttpResponse

import sys
import os
from selenium import webdriver
import path
import time
import re
from bs4 import BeautifulSoup as bs
from bs4 import BeautifulSoup
import pathlib
import joblib
import numpy as np
import json
import gzip
from selenium import webdriver
import path
import time
import signal
import multiprocessing
import sys
import os
import path
import time
import re
from bs4 import BeautifulSoup as bs
import pathlib
import json

# model libs
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
import numpy as np

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

modelDetectProductUrl = joblib.load('detect_product_url_model.sav')
modelDetectCategoryUrl = joblib.load('detect_category_url_model.sav')
modelDetectCategoryPage = joblib.load('detect_category_page_model.sav')